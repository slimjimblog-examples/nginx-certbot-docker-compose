package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
)

var addr string

func main() {
	flag.StringVar(&addr, "addr", ":8080", "http listen address")
	flag.Parse()
	log.Println("Starting rest service on", addr)
	go initApp()
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Kill, os.Interrupt)
	sig := <-sigChan
	log.Println("Caught signal", sig)
	os.Exit(0)
}

func initApp() {
	http.HandleFunc("/", serve)
	http.ListenAndServe(addr, nil)
}

func serve(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello %s, you sent a %s request to %s from %s\n", r.RemoteAddr, r.Method, r.URL.Path, r.UserAgent())
}
